package net.ccmob;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ClientConnection<T, E> implements Runnable {

	private T					clientSocket;
	private boolean				running					= false;
	protected ArrayList<String>	chunks;
	protected int				currentChunkPosition	= 0;

	private Matcher				matcher;

	private ServerHandler<E, T>	serverHandler;

	public ClientConnection(T s, ServerHandler<E, T> serverHandler) {
		this.setClientSocket(s);
		this.setServerHandler(serverHandler);
	}

	protected T getClientSocket() {
		return this.clientSocket;
	}

	private void setClientSocket(T clientSocket) {
		this.clientSocket = clientSocket;
	}

	public boolean isRunning() {
		return this.running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public ServerHandler<E, T> getServerHandler() {
		return this.serverHandler;
	}

	private void setServerHandler(ServerHandler<E, T> serverHandler) {
		this.serverHandler = serverHandler;
	}

	@Override
	public void run() {
		while (this.isRunning()) {
			this.process();
		}
	}

	protected abstract void process();

	protected void handle(String line) {
		System.out.println("Got data: " + line);
		if (this.matches(Constants.getPattern, line)) {
			this.get();
		} else if (this.matches(Constants.initxPattern, line)) {
			String chunkSizeS = this.matcher.group(1);
			System.out.println("ChunkSize: " + chunkSizeS);
			Integer chunkSize = Integer.parseInt(chunkSizeS);
			String filename = this.matcher.group(2);
			this.initx(chunkSize, filename);
			this.sendCommand("OK");
		}
	}

	protected void get() {
		if (this.currentChunkPosition < this.chunks.size()) {
			System.out.println(this.currentChunkPosition + " of " + this.chunks.size());
			this.sendCommand("DATA;" + this.chunks.get(this.currentChunkPosition));
			this.currentChunkPosition++;
		} else {
			System.out.println(this.currentChunkPosition + " of " + this.chunks.size() + ". Done");
			this.finishConnection();
			return;
		}
	}

	protected void initx(int chunkSize, String filename) {
		File f = new File(filename);
		if (!f.exists()) {
			this.sendCommand("ERROR;FileNotFound");
			this.finishConnection();
			return;
		}
		if (chunkSize > Constants.MAX_CHUNK_SIZE) {
			this.sendCommand("ERROR;ChunkSizeToBig_MaxChunk_" + Constants.MAX_CHUNK_SIZE);
			this.finishConnection();
			return;
		}

		try {
			BufferedReader reader = new BufferedReader(new FileReader(f));
			this.chunks = new ArrayList<>();
			StringBuilder builder = new StringBuilder();
			String line = "";
			try {
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			String fileContents = builder.toString();
			int position = 0;
			while (position < fileContents.length()) {
				if (position + chunkSize <= fileContents.length()) {
					this.chunks.add(fileContents.substring(position, position + chunkSize));
				} else {
					this.chunks.add(fileContents.substring(position));
				}
				position += chunkSize;
			}

			System.out.println("FileContents:");
			System.out.println(fileContents);

			System.out.println("Chunks:");
			for (String s : this.chunks) {
				System.out.println(s);
			}

			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			this.sendCommand("ERROR;FileNotFound");
			this.finishConnection();
			return;
		}
	}

	private void finishConnection() {
		this.sendCommand("FINISH");
		this.closeConnection();
	}

	protected abstract void closeConnection();

	protected boolean matches(Pattern p, String l) {
		this.matcher = p.matcher(l);
		return this.matcher.matches();
	}

	protected abstract void sendCommand(String cmd);

	public void start() {
		new Thread(this).start();
	}

}
