package net.ccmob;

public interface ICommandHandler {

	public void handle(String line, ClientConnection c);

}
