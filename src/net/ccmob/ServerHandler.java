package net.ccmob;

public abstract class ServerHandler<T, E> implements Runnable {

	private T		serverSocket;

	private boolean	running	= false;

	public T getServerSocket() {
		return this.serverSocket;
	}

	protected void setServerSocket(T serverSocket) {
		this.serverSocket = serverSocket;
	}

	public boolean isRunning() {
		return this.running;
	}

	protected void setRunning(boolean running) {
		this.running = running;
	}

	public abstract void closeConnection(ClientConnection<E, T> c);

}
