package net.ccmob;

import java.util.regex.Pattern;

public class Constants {

	public static final Pattern	initxPattern	= Pattern.compile("^[I][N][I][T][X][;]([0-9]{1,})[;]([\\w,\\s-]+[.][A-Za-z]{1,})$");

	public static final Pattern	dataPattern		= Pattern.compile("^[D][A][T][A][;](.{1,})");

	public static final Pattern	finishPattern	= Pattern.compile("^[F][I][N][I][S][H]$");

	public static final Pattern	errorPattern	= Pattern.compile("^[E][R][R][O][R][;](.{1,})");

	public static final Pattern	okPattern		= Pattern.compile("^[O][K]$");

	public static final Pattern	getPattern		= Pattern.compile("^[G][E][T]$");

	public static final int		MAX_CHUNK_SIZE	= 1024;

	public static int			UDP_bufferSize	= MAX_CHUNK_SIZE;

	public static final int		SERVER_PORT		= 8266;

	public final static int		TCP				= 1;
	public final static int		UDP				= 2;

}
