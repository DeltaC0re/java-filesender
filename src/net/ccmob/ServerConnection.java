package net.ccmob;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ServerConnection<T> implements Runnable {

	private T					socket;
	private ClientHandler		clientHandler;
	private boolean				running			= false;
	private StringBuilder		receivedData;
	private Matcher				matcher;
	private int					chunkSize;
	private String				fileName;

	private int					serverPort;
	private InetAddress			serverAddress;
	private static final File	outputFolder	= new File("outputs");

	public ServerConnection(T socket, int port, InetAddress serverAddress) {
		this.setSocket(socket);
	}

	public void handle(String line) {
		System.out.println("[C] Got data: " + line);
		if (this.matches(Constants.dataPattern, line)) {
			if (this.receivedData == null) {
				this.receivedData = new StringBuilder();
			}
			this.receivedData.append(this.matcher.group(1));
			this.sendCommand("GET");
		} else if (this.matches(Constants.finishPattern, line)) {
			System.out.println("Received file contents: " + this.receivedData);
			if (!outputFolder.exists()) {
				outputFolder.mkdir();
			}
			File outputFile = new File(outputFolder.toString() + "/" + this.getFileName());
			try {
				FileWriter writer = new FileWriter(outputFile);
				writer.write(this.receivedData.toString());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.closeConnection();
			this.getClientHandler().setRunning(false);
		} else if (this.matches(Constants.errorPattern, line)) {
			System.err.println("An error occured: " + this.matcher.group(1));
			this.getClientHandler().errorHandler(this.matcher.group(1));
		} else if (this.matches(Constants.okPattern, line)) {
			this.sendCommand("GET");
		}
	}

	@Override
	public void run() {
		while (this.isRunning()) {
			this.process();
		}
	}

	public abstract void process();

	private boolean matches(Pattern p, String l) {
		this.matcher = p.matcher(l);
		return this.matcher.matches();
	}

	public abstract void sendCommand(String cmd);

	public abstract void closeConnection();

	protected T getSocket() {
		return this.socket;
	}

	protected void setSocket(T socket) {
		this.socket = socket;
	}

	private boolean isRunning() {
		return this.running;
	}

	protected void setRunning(boolean running) {
		this.running = running;
	}

	protected int getServerPort() {
		return this.serverPort;
	}

	private void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	protected InetAddress getServerAddress() {
		return this.serverAddress;
	}

	private void setServerAddress(InetAddress serverAddress) {
		this.serverAddress = serverAddress;
	}

	private ClientHandler getClientHandler() {
		return this.clientHandler;
	}

	public void setClientHandler(ClientHandler clientHandler) {
		this.clientHandler = clientHandler;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getChunkSize() {
		return this.chunkSize;
	}

	public void setChunkSize(int chunkSize) {
		this.chunkSize = chunkSize;
	}

}
