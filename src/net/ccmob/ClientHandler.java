package net.ccmob;

public abstract class ClientHandler implements Runnable {

	private int					serverPort			= -1;
	private ServerConnection<?>	serverConnection	= null;

	private boolean				running				= false;

	public ClientHandler(int serverPort, ServerConnection<?> con, int chunkSize, String fileName) {
		this.setServerPort(serverPort);
		this.setServerConnection(con);
		this.getServerConnection().setClientHandler(this);
		this.getServerConnection().setFileName(fileName);
		this.getServerConnection().setChunkSize(chunkSize);
		this.setRunning(true);
	}

	protected int getServerPort() {
		return this.serverPort;
	}

	private void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public abstract void errorHandler(String error);

	protected ServerConnection<?> getServerConnection() {
		return this.serverConnection;
	}

	protected void setServerConnection(ServerConnection<?> serverConnection) {
		this.serverConnection = serverConnection;
	}

	@Override
	public void run() {
		new Thread(this.getServerConnection()).start();
		this.getServerConnection().sendCommand("INITX;" + this.getServerConnection().getChunkSize() + ";" + this.getServerConnection().getFileName());
		while (this.isRunning()) {
			;
		}
	}

	private boolean isRunning() {
		return this.running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

}
