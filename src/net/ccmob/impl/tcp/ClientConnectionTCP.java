package net.ccmob.impl.tcp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import net.ccmob.ClientConnection;
import net.ccmob.ServerHandler;

public class ClientConnectionTCP extends ClientConnection<Socket, ServerSocket> {

	private DataOutputStream	clientOutputStream;
	private BufferedReader		clientInputReader;

	public ClientConnectionTCP(Socket s, ServerHandler<ServerSocket, Socket> so) {
		super(s, so);
		try {
			this.setClientOutputStream(new DataOutputStream(s.getOutputStream()));
			this.setClientInputReader(new BufferedReader(new InputStreamReader(s.getInputStream())));
			this.setRunning(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void process() {
		try {
			this.handle(this.getClientInputReader().readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void closeConnection() {
		this.setRunning(false);
		try {
			this.getClientInputReader().close();
			this.getClientSocket().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void sendCommand(String cmd) {
		try {
			System.out.println(">> '" + cmd + "'");
			this.getClientOutputStream().writeBytes(cmd + "\r\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private DataOutputStream getClientOutputStream() {
		return this.clientOutputStream;
	}

	private void setClientOutputStream(DataOutputStream clientOutputStream) {
		this.clientOutputStream = clientOutputStream;
	}

	private BufferedReader getClientInputReader() {
		return this.clientInputReader;
	}

	private void setClientInputReader(BufferedReader clientInputReader) {
		this.clientInputReader = clientInputReader;
	}

}
