package net.ccmob.impl.tcp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

import net.ccmob.ServerConnection;

public class ServerConnectionTCP extends ServerConnection<Socket> {

	private DataOutputStream	serverOutputStream;
	private BufferedReader		serverInputReader;

	public ServerConnectionTCP(int port, InetAddress serverAddress) throws IOException {
		super(new Socket(serverAddress, port), port, serverAddress);
		try {
			this.setServerOutputStream(new DataOutputStream(this.getSocket().getOutputStream()));
			this.setServerInputReader(new BufferedReader(new InputStreamReader(this.getSocket().getInputStream())));
			this.setRunning(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void process() {
		try {
			this.handle(this.getServerInputReader().readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendCommand(String cmd) {
		try {
			System.out.println(">> '" + cmd + "'");
			this.getServerOutputStream().writeBytes(cmd + "\r\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void closeConnection() {
		this.setRunning(false);
		try {
			this.getServerInputReader().close();
			this.getSocket().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private DataOutputStream getServerOutputStream() {
		return this.serverOutputStream;
	}

	private void setServerOutputStream(DataOutputStream serverOutputStream) {
		this.serverOutputStream = serverOutputStream;
	}

	private BufferedReader getServerInputReader() {
		return this.serverInputReader;
	}

	private void setServerInputReader(BufferedReader serverInputReader) {
		this.serverInputReader = serverInputReader;
	}

}
