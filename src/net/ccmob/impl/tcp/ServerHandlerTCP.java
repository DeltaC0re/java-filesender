package net.ccmob.impl.tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import net.ccmob.ClientConnection;
import net.ccmob.Constants;
import net.ccmob.ServerHandler;

public class ServerHandlerTCP extends ServerHandler<ServerSocket, Socket> {

	public ServerHandlerTCP() {
		try {
			this.setServerSocket(new ServerSocket(Constants.SERVER_PORT));
			this.setRunning(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			this.getServerSocket().setSoTimeout(1000);
			while (this.isRunning()) {
				try {
					new ClientConnectionTCP(this.getServerSocket().accept(), this).start();
					System.out.println("New connection.");
				} catch (Exception e) {
					if (!(e instanceof IOException)) {
						e.printStackTrace();
					}
				}
			}
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void closeConnection(ClientConnection<Socket, ServerSocket> c) {}

}
