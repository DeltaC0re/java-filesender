package net.ccmob.impl.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import net.ccmob.ServerConnection;

public class ServerConnectionUDP extends ServerConnection<DatagramSocket> {

	private DatagramPacket			packet				= null;
	private DatagramPacket			receivedPacket		= null;
	ClientConnetionInformationUDP	serverConnection	= null;

	public ServerConnectionUDP(int port, InetAddress serverAddress) throws SocketException {
		super(null, port, serverAddress);
		this.setRunning(true);
		this.serverConnection = new ClientConnetionInformationUDP(serverAddress, port);
		this.setSocket(new DatagramSocket(port + 1));
	}

	@Override
	public void process() {
		try {
			this.handle(this.serverConnection.getCommandBuffer().take());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendCommand(String cmd) {
		this.packet = new DatagramPacket(cmd.getBytes(), cmd.getBytes().length);
		try {
			this.getSocket().send(this.packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void closeConnection() {
		this.setRunning(false);
		this.getSocket().close();
	}

}
