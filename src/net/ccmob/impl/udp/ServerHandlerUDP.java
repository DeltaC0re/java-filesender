package net.ccmob.impl.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import net.ccmob.ClientConnection;
import net.ccmob.Constants;
import net.ccmob.ServerHandler;

public class ServerHandlerUDP extends ServerHandler<DatagramSocket, ClientConnetionInformationUDP> {

	public ServerHandlerUDP() {
		try {
			this.setServerSocket(new DatagramSocket(Constants.SERVER_PORT));
			this.setRunning(true);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (this.isRunning()) {
			byte[] data = new byte[Constants.UDP_bufferSize];
			DatagramPacket clientPacket = new DatagramPacket(data, Constants.UDP_bufferSize);
			try {
				this.getServerSocket().receive(clientPacket);
				ClientConnetionInformationUDP clientConnection = new ClientConnetionInformationUDP(clientPacket.getAddress(), clientPacket.getPort());
				new ClientConnectionUDP(clientConnection, this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void closeConnection(ClientConnection<ClientConnetionInformationUDP, DatagramSocket> c) {
		c.setRunning(false);
	}
}
