package net.ccmob.impl.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import net.ccmob.ClientConnection;
import net.ccmob.Constants;
import net.ccmob.ServerHandler;

public class ClientConnectionUDP extends ClientConnection<ClientConnetionInformationUDP, DatagramSocket> {

	private DatagramPacket packet = null;

	public ClientConnectionUDP(ClientConnetionInformationUDP p, ServerHandler<DatagramSocket, ClientConnetionInformationUDP> s) {
		super(p, s);
	}

	@Override
	protected void process() {
		try {
			this.handle(this.getClientSocket().getCommandBuffer().take());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void closeConnection() {
		this.setRunning(false);
		this.getServerHandler().closeConnection(this);
	}

	@Override
	protected void sendCommand(String cmd) {
		this.packet = new DatagramPacket(cmd.getBytes(), cmd.getBytes().length, this.getClientSocket().getClientAddress(), this.getClientSocket().getClientPort());
		try {
			this.getServerHandler().getServerSocket().send(this.packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void initx(int chunkSize, String filename) {
		super.initx(chunkSize, filename);
		if (Constants.UDP_bufferSize < chunkSize + 7) {
			Constants.UDP_bufferSize = chunkSize + 8;
		}
	}

}
