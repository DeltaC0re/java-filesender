package net.ccmob.impl.udp;

import java.net.InetAddress;
import java.util.concurrent.ArrayBlockingQueue;

public class ClientConnetionInformationUDP {

	private int							clientPort		= -1;
	private InetAddress					clientAddress	= null;

	private ArrayBlockingQueue<String>	commandBuffer	= null;

	public ClientConnetionInformationUDP(InetAddress ip, int port) {
		this.setClientAddress(ip);
		this.setClientPort(port);
		this.setCommandBuffer(new ArrayBlockingQueue<>(16));
	}

	public int getClientPort() {
		return this.clientPort;
	}

	private void setClientPort(int clientPort) {
		this.clientPort = clientPort;
	}

	public InetAddress getClientAddress() {
		return this.clientAddress;
	}

	private void setClientAddress(InetAddress clientAddress) {
		this.clientAddress = clientAddress;
	}

	public ArrayBlockingQueue<String> getCommandBuffer() {
		return this.commandBuffer;
	}

	private void setCommandBuffer(ArrayBlockingQueue<String> commandBuffer) {
		this.commandBuffer = commandBuffer;
	}

}
