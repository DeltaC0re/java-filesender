package net.ccmob.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

import net.ccmob.ClientHandler;
import net.ccmob.Constants;
import net.ccmob.ServerConnection;
import net.ccmob.impl.tcp.ServerConnectionTCP;
import net.ccmob.impl.udp.ServerConnectionUDP;

public class Client {

	ClientHandler		clientHandler;
	Thread				clientThread;
	ServerConnection<?>	conn	= null;

	public static void main(String[] args) {
		new Client();
	}

	public Client() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		InetAddress ip;
		try {
			ip = InetAddress.getByName("localhost");
			try {
				System.out.println("Filename:");
				String fileName = reader.readLine();
				System.out.println("Chunk size (<=1024):");
				int chunkSize = Integer.parseInt(reader.readLine());
				System.out.println("1: TCP | 2: UDP");
				int choice = Integer.parseInt(reader.readLine());
				switch (choice) {
					case Constants.TCP: {
						System.out.println("You chose TCP!");
						this.conn = new ServerConnectionTCP(Constants.SERVER_PORT, ip);
						break;
					}
					case Constants.UDP: {
						System.out.println("You chose UDP!");
						this.conn = new ServerConnectionUDP(Constants.SERVER_PORT, ip);
						break;
					}
					default: {
						System.err.println("You skrub chose the not available option. deal with it. Formating C ...");
						System.exit(1);
					}
				}
				if (this.conn != null) {
					this.clientHandler = new ClientHandler(Constants.SERVER_PORT, this.conn, chunkSize, fileName) {
						@Override
						public void errorHandler(String error) {
							System.err.println("WHOHOOOO! AN ERROR: " + error);
						}
					};
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (UnknownHostException e1) {
			System.err.println("You skrub chose to hardcode the server addres. deal with it. Formating C ...");
			System.exit(1);
			e1.printStackTrace();
		}

		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.clientThread = new Thread(this.clientHandler, "client-thread");
		this.clientThread.start();
	}

}
