package net.ccmob.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import net.ccmob.Constants;
import net.ccmob.ServerHandler;
import net.ccmob.impl.tcp.ServerHandlerTCP;
import net.ccmob.impl.udp.ServerHandlerUDP;

public class Server {

	ServerHandler<?, ?>	serverHandler;
	Thread				serverThread;

	public static void main(String[] args) {
		new Server();
	}

	public Server() {
		System.out.println("1: TCP | 2: UDP");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try {
			int choice = Integer.parseInt(reader.readLine());
			switch (choice) {
				case Constants.TCP: {
					System.out.println("You chose TCP!");
					this.serverHandler = new ServerHandlerTCP();
					break;
				}
				case Constants.UDP: {
					System.out.println("You chose UDP!");
					this.serverHandler = new ServerHandlerUDP();
					break;
				}
				default: {
					System.err.println("You skrub chose the not available option. deal with it. Formating C ...");
					System.exit(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.serverThread = new Thread(this.serverHandler, "server-thread");
		this.serverThread.start();
	}

}
